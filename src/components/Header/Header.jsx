import React, {useEffect, useState} from 'react'
import './Header.css'
import {Icon, Container, Header} from "semantic-ui-react";


const MainHeader = ({messages}) => {
    const [countOfUsers, setCountOfUsers] = useState(0);
    const [countOfMessages, setCountOfMessages] = useState(0);
    const [lastMessageDate, setLastMessageDate] = useState('');

    useEffect(()=>{
        dataAnalysis();
    },[messages])

    function dataAnalysis() {
        if (messages.length > 0) {
            let countOfMessages = 0;
            let set = new Set();
            messages.forEach(message => {
                set.add(message.userId);
                countOfMessages++;
            })
            setCountOfUsers(set.size);
            setCountOfMessages(countOfMessages);
            const lastDate = messages[messages.length-1].createdAt;
            setLastMessageDate(lastDate);
        } else {
            return
        }
    }

    return (
        <div className='header my-header'>
            <Container className='header-container'>
            <div className='header-title'>
                <Header as='h2'>
                    <Icon name='plug'/>
                    <Header.Content>My chat</Header.Content>
                </Header>
            </div>
            <div className='header-users-count'>
                <Icon name='user'/>
                {countOfUsers} participants
            </div>
            <div className='header-messages-count'>
                <Icon name='mail'/>
                {countOfMessages} messages
            </div>
            <div className='header-last-message-date'>
                <Icon name='calendar'/>
                Last message at:
                <br/>
                {lastMessageDate}
            </div>
            </Container>
        </div>
    )
}

export default MainHeader