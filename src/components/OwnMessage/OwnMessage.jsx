import React, {useState} from 'react'
import {Comment, Icon, Input} from "semantic-ui-react";
import './OwnMessage.css'


const OwnMessage = ({message, likeMessage, deleteMessage, updateMessage}) => {
    const [isEditModeActivated, setIsEditModeActivated] = useState(false);
    const [newMessage, setNewMessage] = useState(message.text);
    const [isLiked, setIsLiked] = useState(false);

    const clickLikeHandler = () => {
        likeMessage(message.id);
        setIsLiked(!isLiked);
    }

    const clickDeleteHandler = () => {
        deleteMessage(message.id);
    }

    const editModeActivated =() => {
        setIsEditModeActivated(true)
    }

    const changeMessage = (e) => {
        setNewMessage(e.target.value)
    }

    const handleUpdateMessage = () => {
        setIsEditModeActivated(false)
        updateMessage(message.id, newMessage)
    }

    return (
        <div className='own-message'>
            <Comment.Group>
                <Comment>
                    <Comment.Avatar as='a' className='message-user-avatar' src={message.avatar}/>
                    <Comment.Content>
                        <Comment.Author className='message-user-name'>{message.user}</Comment.Author>
                        {!isEditModeActivated &&
                            <Comment.Text className='message-text'>
                                {message.text}
                            </Comment.Text>
                        }
                        {isEditModeActivated &&
                        <Comment.Text className='message-text'>
                            <Input
                                fluid
                                autoFocus={true}
                                type="text"
                                onChange={changeMessage}
                                onBlur={handleUpdateMessage}
                                defaultValue={message.text}
                            />
                        </Comment.Text>
                        }
                        <Comment.Actions>
                            <Comment.Action
                                style={{marginRight: 0}}
                                onClick={clickLikeHandler}
                                className={isLiked ? 'message-liked' : 'message-like'}><Icon
                                name='like'/>
                            </Comment.Action>
                            <Comment.Metadata style={{width: '10px', marginLeft: 0, marginRight: '5px'}}>
                                <span>{message.likesCount ? message.likesCount : null}</span>
                            </Comment.Metadata>
                            <Comment.Action
                                className='message-edit'
                                onClick={editModeActivated}
                            >
                                <Icon name='edit'/>
                            </Comment.Action>
                            <Comment.Action
                                className='message-delete'
                                onClick={clickDeleteHandler}
                            >
                                <Icon name='delete'/>
                            </Comment.Action>
                        </Comment.Actions>
                        <Comment.Metadata style={{marginLeft: 0}}>
                            <div className='message-time'>{message.createdAt.split(' ')[1]}</div>
                        </Comment.Metadata>
                    </Comment.Content>
                </Comment>
            </Comment.Group>


        </div>
    )
}

export default OwnMessage;