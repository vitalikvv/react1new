import React, {useState} from 'react'
import {Container, Form} from "semantic-ui-react";
import './MessageInput.css'

const MessageInput = ({addNewMessage}) => {
    const [value, setValue] = useState('')

    const changeHandler = (e) => {
        setValue(e.target.value)
    }

    const clickHandler = () => {
        if (value.length > 0) {
            addNewMessage(value)
            setValue('')
        }
    }

    return (
        <div className='message-input'>
            <Container text>
                <Form>
                    <Form.TextArea
                        value={value}
                        onChange={changeHandler}
                        className='message-input-text'
                        label='Input your message'
                        placeholder='Tell us something...'
                    />
                    <Form.Button
                        className='message-input-button'
                        onClick={clickHandler}
                    >
                        Submit
                    </Form.Button>
                </Form>
            </Container>
        </div>
    )
}

export default MessageInput