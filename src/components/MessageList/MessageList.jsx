import React from 'react'
import {Container, Divider} from 'semantic-ui-react'
import './MessageList.css'
import Message from "../Message/Message";
import OwnMessage from "../OwnMessage/OwnMessage";
import {getMonth, getWeekDay} from "../../helpers/changeDateFormat";


const MessageList = ({messages, currentRandomUser, likeMessage, deleteMessage, updateMessage}) => {
    let date = '';
    const now = new Date();
    const year = now.getFullYear()
    const month = `${now.getMonth() + 1}`.padStart(2, 0)
    const day = `${now.getDate()}`.padStart(2, 0)
    const currentDate = [day, month, year].join("-")
    const millisecondsInDay = 86400000;
    return (
        <div className='message-list'>
            <Container text style={{marginTop: '7em'}}>
                {messages.map(message => {
                        if (date !== message.createdAt.split(' ')[0]) {
                            date = message.createdAt.split(' ')[0]

                            const dateParts = message.createdAt.split(' ')[0].split('-');
                            const dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
                            const timeDiff = new Date(currentDate) - new Date(dateObject);
                            console.log(new Date(currentDate))
                            console.log(new Date(dateObject))
                            let dateToDisplay = getWeekDay(new Date(dateObject)) + `, ${dateParts[0]} ` + getMonth(new Date(dateObject))
                            console.log(dateToDisplay)
                            if (timeDiff < millisecondsInDay) {
                                dateToDisplay = 'Today'
                            }
                            if (timeDiff >= millisecondsInDay && timeDiff < millisecondsInDay*2) {
                                dateToDisplay = 'Yesterday'
                            }

                            return (
                                <div key={message.id}>
                                    <Divider className='messages-divider' horizontal>{dateToDisplay}</Divider>
                                    {message.userId === currentRandomUser
                                        ? <OwnMessage
                                            likeMessage={likeMessage}
                                            message={message}
                                            deleteMessage={deleteMessage}
                                            updateMessage={updateMessage}
                                        />
                                        : <Message message={message} likeMessage={likeMessage}/>}
                                </div>
                            )
                        } else {
                            return (
                                <div key={message.id}>
                                    {message.userId === currentRandomUser
                                        ? <OwnMessage
                                            likeMessage={likeMessage}
                                            message={message}
                                            deleteMessage={deleteMessage}
                                            updateMessage={updateMessage}
                                        />
                                        : <Message message={message} likeMessage={likeMessage}/>}
                                </div>
                            )
                        }
                    }
                )}
            </Container>
        </div>
    )
}

export default MessageList