
const API_URL = 'https://edikdolynskyi.github.io/react_sources/messages.json';

export function callApi() {
    const url = API_URL;
    const options = {
        method: 'GET',
    };
    return fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .catch((error) => {
            throw error;
        });
}