
export function changeDateFormat(messages) {

    messages.sort(function(a,b){
        const c = new Date(a.createdAt).valueOf();
        const d = new Date(b.createdAt).valueOf();
        return c-d;
    });

    messages.forEach(message => {
        const cangedDate = message.createdAt.split('T').join(' ').split('.')[0]
        const yyyymmddArray = cangedDate.split(' ')[0].split('-')
        const ddmmyyyy = yyyymmddArray.reverse().join('-')
        const hhmm = cangedDate.split(' ')[1].substr(0, 5)
        message.createdAt = ddmmyyyy + ' ' + hhmm;
    })

    return messages;
}


export function getWeekDay(date) {
    let days = ['Sunday', 'Monday', 'Thursday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    return days[date.getDay()];
}

export function getMonth(date) {
    let days = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    return days[date.getMonth()];
}